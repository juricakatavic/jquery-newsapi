$(document).ready(function () {

  /* news API */

  $("#searchbtn").on("click", function (e) {
    e.preventDefault();

    let query = $("#searchquery").val();
    let url = "https://newsapi.org/v2/top-headlines?q=" + query + "&country=us&category=business&pageSize=5&apiKey=5711c9c34fc44673bcd67deac6f3af72";

    if (query !== "") {
      $.ajax({

        url: url,
        methode: "GET",
        dataType: "json",

        success: function (news) {
          let output = "";
          let latestNews = news.articles;
          

          for (let i in latestNews) {
            output += `
              <div class="content-news">
                <div class="content-news-img" style="background-image: url('${latestNews[i].urlToImage}')">
                  </div>
                  <div class="content-news-text">
                      <h3>${latestNews[i].title}</h3>
                      <p>Author: ${latestNews[i].author}</p>
                      <p id="content">${latestNews[i].content}</p>
                      <a class="link" href="${latestNews[i].url}">Read more</a>
                  </div>
              </div>               
            `;
          }

          if (output !== "") {
            $("#newsResults").html(output);
            $(".arrow").removeClass("hide").addClass("show");
            $(".content-news").first().addClass("active");
            /*
            let bg = $(".content-news-img").css('background-image');
            if (bg == null) {
               $('.content-news-img').css("background-image", "url(./images/placeholder.jpeg)");
            }
            */
          } else {
            let newsnotFound = "Sorry, no news found on this topic"
            $("#newsResults").html("<div class='notfound'>" + newsnotFound + "</div>");
            $(".arrow").addClass("hide").removeClass("show");
          }
        },

        error: function () {
          console.log("Some error occured");
        }
      });

    } else {
      let wrightSomething = "Please enter search!"
      $("#newsResults").html("<div class='notfound'>" + wrightSomething + "</div>");
    }
  });


  /*slider functions*/

  $("#next").click(function () {
    let activeDiv = $("div.active");
    activeDiv.removeClass("active");
    if (activeDiv.next().length == 0) {
      $("div.content-news").eq(0).addClass("active");
    } else {
      activeDiv.next().addClass("active");
    }
  });

  $("#prev").click(function () {
    let activeDiv = $("div.active");
    activeDiv.removeClass("active");
    if (activeDiv.prev().length == 0) {
      $("div.content-news").eq(-1).addClass("active");
    } else {
      activeDiv.prev().addClass("active");
    }
  });
});

